const express = require("express");
const app = express();
const cors = require("cors");
const data = require("./data/data.json");

app.use(cors());

app.get("/decimalOddsMoreThanTwo", (_req, res) => {
  const moreThanTwo = data.bets.filter((bet) =>
    bet.odds.every((odd) => odd.oddsDecimal >= 2)
  );
  res.json(moreThanTwo);
});

app.get("/decimalOddsLessThanTwo", (_req, res) => {
  const lessThanTwo = data.bets.filter((bet) =>
    bet.odds.every((odd) => odd.oddsDecimal < 2)
  );
  res.send(lessThanTwo);
});

app.listen(4000, () => {
  console.log("Example app listening on port 4000!");
});
