import styled from "styled-components";

interface Props {
  center: boolean;
}

export const Container = styled.div`
  background-color: #dce5e5;
  height: 100vh;
  width: 100vw;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Header = styled.div`
  border-bottom: 1px solid grey;
  padding: 0.5rem;
  display: flex;

  h2 {
    font-size: 1.2rem;
    margin: 0;
  }

  select {
    margin-left: auto;
  }
`;

export const Wrapper = styled.div<Props>`
  background-color: #f2f2f2;
  width: 60vw;
  min-height: 30vh;
  min-width: 300px;
  display: flex;
  flex-direction: column;
  border-radius: 5px;
  box-shadow: 0 0 10px #ccc;
  align-items: ${(props) => (props.center ? "center" : "initial")};
`;

export const FormWrapper = styled.div`
  padding: 0.5rem;
  flex: 1;

  form {
    display: flex;
    flex-direction: column;

    button {
      margin-left: auto;
    }
  }
`;

export const BetInfo = styled.div`
  background: white;
  display: flex;
  align-items: center;
  margin-bottom: 10px;
  border: 1px solid grey;
  padding: 0.5rem;
  border-radius: 5px;
  box-shadow: 0 0 5px #ccc;

  p {
    margin: 5px 0;
  }

  input {
    margin-left: auto;
    padding: 0.5rem;
  }

  button {
    font-size: 1rem;
    padding: 0.5rem 1rem;
    background: #94c994;
    border-radius: 3px;
    border: 1px solid grey;
  }
`;

export const StatusParagraph = styled.p`
  text-align: center;
`;

export const ValidationParagraph = styled.p`
  text-align: center;
  color: tomato;
`;

export const TotalAmount = styled.p`
  font-size: 1.5rem;

  span {
    color: white;
    background: #38ddc7;
    padding: 0.5rem 1rem;
    border-radius: 5px;
  }
`;
