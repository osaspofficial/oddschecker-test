import React from "react";
import "./App.css";
import Betslip from "./views/Betslip.tsx";
import Receipt from "./views/Receipt.tsx";
import { Container } from "./Styles.tsx";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

const App: React.FC = () => {
  return (
    <Container>
      <Router>
        <Routes>
          <Route path="/" exact element={<Betslip />} />
          <Route path="/receipt" exact={true} element={<Receipt />} />
        </Routes>
      </Router>
    </Container>
  );
};

export default App;
