import { IQueryProps, IonFormSubmit, Bet, IFormValues } from "../../types";
import { BetInfo, ValidationParagraph } from "../../Styles.tsx";
import { getBestOddsDecimal } from "../../helpers.ts";

interface Props extends IQueryProps {
  onFormSubmit: IonFormSubmit;
  data: Bet[];
  onInputchange: (event, id) => void;
  isFormValid: boolean;
  formValues: IFormValues;
}

const BetForm: React.FC<Props> = ({
  data,
  onFormSubmit,
  onInputchange,
  isFormValid,
  formValues,
}) => {
  return (
    <form onSubmit={(event) => onFormSubmit(event)} data-testid={"bet_form"}>
      {data.map(({ name, betId, odds }) => (
        <BetInfo key={betId} data-testid="bet_info">
          <div>
            <p data-testid="bet_name">Bet name is: {name}</p>
            <p data-testid="bet_odds">
              best odds:{" "}
              <span data-testid="best_odds">{getBestOddsDecimal(odds)}</span>
            </p>
          </div>

          <input
            data-testid={`${betId}`}
            id={betId}
            name={betId}
            type="number"
            placeholder="Enter stake"
            onChange={(event) => onInputchange(event, betId)}
            defaultValue={formValues[betId] ? formValues[betId] : ""}
          />
        </BetInfo>
      ))}

      {!isFormValid && (
        <ValidationParagraph data-testid="validation_paragraph">
          Please enter correct values in the fields above!
        </ValidationParagraph>
      )}
      <button type="submit" data-testid="bet_now_btn">
        Bet Now
      </button>
    </form>
  );
};

export default BetForm;
