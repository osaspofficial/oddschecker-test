import { Wrapper } from "../../Styles.tsx";

interface Props {
  children: React.ReactNode;
  center?: boolean;
  testId?: string;
}

const Layout: React.FC<Props> = ({ children, center, testId }) => {
  return (
    <Wrapper id={testId} center={center}>
      {children}
    </Wrapper>
  );
};

export default Layout;
