import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Dropdown from "./Dropdown";
import { IFilterOptions } from "../../types";

describe("The Dropdown component", () => {
  const mockOptions: IFilterOptions[] = [
    {
      name: "nameString1",
      value: "valueString1",
    },
    {
      name: "nameString2",
      value: "valueString2",
    },
  ];

  it("renders component without errors", async () => {
    const mockedOnChange = jest.fn();
    const dropdownComponent = render(
      <Dropdown
        options={mockOptions}
        onOddsFilterChange={mockedOnChange}
        selected=""
      />
    );

    expect(dropdownComponent).toBeTruthy();
  });

  it("check onchange when option is selected", async () => {
    const mockTest = jest.fn();

    const { getByTestId, debug } = render(
      <Dropdown
        options={mockOptions}
        onOddsFilterChange={mockTest}
        selected=""
      />
    );
    const select = getByTestId("dropdown");
    const options = screen.getAllByTestId("dropdown_option");
    const firstOption = options[0].textContent;

    await userEvent.selectOptions(select, [firstOption]);

    expect(mockTest).toHaveBeenCalledTimes(1);
  });
});
