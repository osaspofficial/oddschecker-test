import { IFilterOptions } from "../../types.d.ts";

interface Props {
  onOddsFilterChange: (event) => void;
  options: IFilterOptions[];
  selected: string;
}

const Dropdown: React.FC<Props> = ({
  options,
  onOddsFilterChange,
  selected,
}) => (
  <select
    onChange={(event) => onOddsFilterChange(event)}
    data-testid="dropdown"
    value={selected}
  >
    {options.map(({ name, value }) => (
      <option value={name} key={name} data-testid="dropdown_option">
        {value}
      </option>
    ))}
  </select>
);

export default Dropdown;
