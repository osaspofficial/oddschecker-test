import { Odd, IFormValues } from "./types.d.ts";

export const getBestOddsDecimal = (odds: Odd[]): string => {
  const oddsDecimalsArr: number[] = odds.map((odd) => odd.oddsDecimal);
  return Math.max(...oddsDecimalsArr).toString();
};

export const getTotalStake = (obj: IFormValues): number => {
  let totalStake = 0;
  for (const property in obj) {
    totalStake += +obj[property];
  }
  return totalStake;
};
