export interface Odd {
  bookmakerCode: string;
  eachWayTerms: string;
  clickoutUrl: string;
  oddsFractional: string;
  oddsDecimal: number;
  oddsUS: number;
  bookmakerBetId: string;
  suspended: boolean;
}

export interface Bet {
  name: string;
  genericName: string;
  bestOddsBookmakers: string;
  betId: any;
  map: string;
  odds: Odd[];
}

export interface IQueryProps {
  isLoading?: boolean;
  error?: any;
  data: Bet[];
}

export interface IFilterOptions {
  name: string;
  value: string;
}

export interface IFormValues {
  [key: number]: string;
}

// export type IonFormSubmit = (formValues: IFormValues) => void;
export type IonFormSubmit = (event: React.FormEvent<HTMLFormElement>) => void;
