const getApi = async (oddsEndpoint) => {
  const response = await fetch(`http://localhost:4000/${oddsEndpoint}`);
  return await response.json();
};

export default getApi;
