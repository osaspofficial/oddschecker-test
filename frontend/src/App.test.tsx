import { render, waitFor, fireEvent } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import React from "react";
import "@testing-library/jest-dom";
import App from "./App.tsx";
import { QueryClient, QueryClientProvider } from "react-query";
import Dropdown from "./components/Dropdown/Dropdown.tsx";

const setup = () => {
  const queryClient = new QueryClient();

  const utils = render(
    <QueryClientProvider client={queryClient}>
      <App />
    </QueryClientProvider>
  );

  return utils;
};

//Integration tests
describe("The Betting flow", () => {
  describe("unhappy path", () => {
    it("shows the validation message when wrong values are entered", async () => {
      // render the app
      const user = userEvent.setup();
      const utils = setup();

      await waitFor(() => expect(utils.container).toHaveTextContent("Betslip"));

      // get all page elems
      const betInputFields = utils.getAllByPlaceholderText("Enter stake");
      const betNowButton = utils.getByTestId("bet_now_btn");

      // for each input, fill out a value
      betInputFields.forEach((input) => {
        // fill out the text input with strings which should fail
        fireEvent.change(input, { target: { value: "abc" } });
      });

      await user.click(betNowButton);

      // wait for validation message
      await waitFor(() =>
        expect(utils.getByTestId("validation_paragraph")).toBeTruthy()
      );
    });
  });

  describe("The happy path", () => {
    it("renders <App /> without errors", async () => {
      const queryClient = new QueryClient();

      const AppComponent = render(
        <QueryClientProvider client={queryClient}>
          <App />
        </QueryClientProvider>
      );

      expect(AppComponent).toBeTruthy();
    });

    it("shows right content when dropdown is updated", async () => {
      // render the app
      const utils = setup();
      await waitFor(() => expect(utils.container).toHaveTextContent("Betslip"));

      const dropdown = utils.getByTestId("dropdown");

      await userEvent.selectOptions(dropdown, ["less than 2"]);

      // wait for page to stop loading after update
      await waitFor(() =>
        expect(utils.container).not.toHaveTextContent("Loading")
      );

      const betOdds = utils.getAllByTestId("best_odds");
      betOdds.forEach((bet) => {
        const betString = bet.textContent;

        // check odds on bets to see that they match expectations
        expect(+betString).toBeLessThan(2);
      });
    });

    it("renders the receipt view if form is filled with the right values (44)", async () => {
      // render the app
      const user = userEvent.setup();
      const utils = setup();

      await waitFor(() => expect(utils.container).toHaveTextContent("Betslip"));

      // get all page elems
      const betInputFields = utils.getAllByPlaceholderText("Enter stake");
      const betNowButton = utils.getByTestId("bet_now_btn");

      // for each input, fill out the forms for each ipit
      betInputFields.forEach((input) => {
        // fill out the text input
        fireEvent.change(input, { target: { value: 22 } });

        expect(input).toHaveValue(22);
      });

      await user.click(betNowButton);

      // wait for the receipt and render the right values
      await waitFor(() => expect(utils.baseElement).toHaveTextContent("44"));
    });
  });
});
