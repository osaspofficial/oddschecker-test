import Layout from "../components/Layout/Layout.tsx";
import { TotalAmount } from "../Styles.tsx";
import { useLocation } from "react-router";

const Receipt: React.FC<{ betTotal: number }> = () => {
  const location = useLocation();
  const betTotal = location.state;

  return (
    <Layout center>
      <h2>Receipt</h2>
      <p>Your bet has been placed!</p>
      <TotalAmount>
        Total Stake: <span>${betTotal}</span>
      </TotalAmount>
    </Layout>
  );
};

export default Receipt;
