import React, { useState } from "react";
import { useNavigate } from "react-router";
import {
  IFormValues,
  IonFormSubmit,
  IQueryProps,
  IFilterOptions,
} from "../types";
import { useQuery } from "react-query";
import Dropdown from "../components/Dropdown/Dropdown.tsx";
import BetForm from "../components/BetForm/BetForm.tsx";
import Layout from "../components/Layout/Layout.tsx";
import getApi from "../services/bet.service.tsx";
import { getTotalStake } from "../helpers.ts";
import { Header, StatusParagraph, FormWrapper } from "../Styles.tsx";
import {
  DECIMALODDSMORETHANTWO,
  DECIMALODDSLESSTHANTWO,
} from "../constants.ts";

const filterOptions: IFilterOptions[] = [
  {
    name: DECIMALODDSMORETHANTWO,
    value: "more than 2",
  },
  {
    name: DECIMALODDSLESSTHANTWO,
    value: "less than 2",
  },
];

const Betslip: React.FC = () => {
  const navigate = useNavigate();
  const [isFormValid, setIsFormValid] = useState<boolean>(true);
  const [formValues, setFormValues] = useState<IFormValues>({});
  const [oddsEndpoint, setOddsEndpoint] = useState<string>(
    DECIMALODDSMORETHANTWO
  );

  const { isLoading, error, data }: IQueryProps = useQuery(oddsEndpoint, () =>
    getApi(oddsEndpoint)
  );

  const onOddsFilterChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ): void => {
    const value = event.target.value;
    setOddsEndpoint(value);
  };

  const onInputchangeHandler = (event, betId) => {
    const value = event.target.value;
    setFormValues((prev) => ({ ...prev, [betId]: value }));
  };

  const onFormSubmit: IonFormSubmit = (event) => {
    event.preventDefault();
    const inputTotal = getTotalStake(formValues);
    if (inputTotal > 0) {
      navigate("/receipt", {
        state: inputTotal,
      });
    } else {
      setIsFormValid(false);
    }
  };

  return (
    <Layout data-testid="betslip-page-wrapper">
      {error && <StatusParagraph>Something went wrong ..." </StatusParagraph>}
      {isLoading && <StatusParagraph>Loading ...</StatusParagraph>}
      {!isLoading && !error && data && (
        <>
          <Header>
            <h2>Betslip</h2>
            <Dropdown
              options={filterOptions}
              onOddsFilterChange={onOddsFilterChange}
              selected={oddsEndpoint}
            />
          </Header>
          <FormWrapper>
            <BetForm
              data={data}
              onFormSubmit={onFormSubmit}
              isFormValid={isFormValid}
              onInputchange={onInputchangeHandler}
              formValues={formValues}
            />
          </FormWrapper>
        </>
      )}
    </Layout>
  );
};

export default Betslip;
